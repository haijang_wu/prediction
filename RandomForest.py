from sklearn.ensemble import RandomForestClassifier
def read_label():
    uid_label = {}
    train_file = open("train_y.csv", "r");
    count = 0
    for line in train_file:
        count = count + 1
        if count>1:
            line_arr = line.strip().split(",");
            uid = line_arr[0]
            label = int(line_arr[1])
            uid_label[uid] = label
    train_file.close()
    return uid_label
def read_feature_type():
    feature_type = []
    feature_type_file = open('features_type.csv', 'r')
    for line in feature_type_file:
        line_arr = line.strip().split(",");
        if line_arr[1]=="\"numeric\"":
            feature_type.append('n')
        elif line_arr[1]=="\"category\"":
            feature_type.append('c')
        else:
            feature_type.append('t')
    feature_type_file.close()
    return feature_type

def main():
    data = []
    label = []
    uid_label = read_label()
    train_file = open("train_x.csv", 'r');
    count = 0
    for line in train_file:
        count = count + 1
        if count>1:
            line_arr = line.strip().split(",");
            uid = line_arr[0]
            feature = []
            for idx in range(1, len(line_arr)):
                if line_arr[idx][0]=='"':
                    tmp = line_arr[idx][1:-1]
                else:
                    tmp = line_arr[idx]
                feature.append(float(tmp))
            data.append(feature)
            label.append(uid_label[uid])
    train_file.close()
    model = RandomForestClassifier(n_estimators=3000, max_depth=None, 
                            min_samples_split=1, min_samples_leaf=50, 
                            max_features="auto",n_jobs=-1)
    model.fit(data,label)
    
    result = open("result.txt","w+")
    result.write('"uid","score"\n')
    
    test_file = open("test_x.csv", 'r');
    tcount = 0
    for tline in test_file:
        tcount = tcount + 1
        if tcount>1:
            tline_arr = tline.strip().split(",");
            uid = tline_arr[0]
            tfeature = []
            for idx in range(1, len(tline_arr)):
                if tline_arr[idx][0]=='"':
                    ttmp = tline_arr[idx][1:-1]
                else:
                    ttmp = tline_arr[idx]
                tfeature.append(float(ttmp))
            score = model.predict_proba(tfeature)[0][1]
            rline = uid+','+str(score)+'\n'
            result.write(rline)          
    test_file.close()    
    result.close()
 
if __name__ == '__main__':
    main()